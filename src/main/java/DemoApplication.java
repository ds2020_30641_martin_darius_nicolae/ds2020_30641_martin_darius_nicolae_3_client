import org.springframework.boot.autoconfigure.SpringBootApplication;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import configuration.TakeMedicationService;
import configuration.TakingExceptions;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

import java.util.List;
import java.util.UUID;

@Configuration
public class DemoApplication {

    public static TakeMedicationService takeMedicationService;

    @Bean
    public HttpInvokerProxyFactoryBean invoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl("http://localhost:8080/taking");
        invoker.setServiceInterface(TakeMedicationService.class);
        return invoker;
    }

    public static void main(String[] args) throws TakingExceptions {
        takeMedicationService = SpringApplication
                .run(DemoApplication.class, args)
                .getBean(TakeMedicationService.class);

        System.setProperty("java.awt.headless", "false");

        List<MedicationPlanDTO> list=takeMedicationService.takeAMed(UUID.fromString("3827758a-1e7a-4dde-8b21-6e411317e13c"));
        View myView=new View(list);

        //in listerenul de la buton trebuie sa imi apelez Client.takeMedicationService.metodaPeCareOFac(
        //for(MedicationPlanDTO med:list){
        //    System.out.println(med.getName());
        //}
    }
}