package ro.tuc.ds2020.dtos;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MedicationPlanDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private UUID id_medicationPlan;
    private String name;
    private String intakeIntervals;
    private String period;
    private List<MedicationDTO> listOfMedications;
    private PatientDTO patientDTO;


    public MedicationPlanDTO(){

    }

    public MedicationPlanDTO(UUID id_medicationPlan,String name, String intakeIntervals, String period, List<MedicationDTO> listOfMedications,PatientDTO patientDTO) {
        this.id_medicationPlan = id_medicationPlan;
        this.name=name;
        this.intakeIntervals = intakeIntervals;
        this.period = period;
        this.listOfMedications = listOfMedications;
        this.patientDTO=patientDTO;

    }

    public UUID getId_medicationPlan() {
        return id_medicationPlan;
    }

    public void setId_medicationPlan(UUID id_medicationPlan) {
        this.id_medicationPlan = id_medicationPlan;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public List<MedicationDTO> getListOfMedications() {
        return listOfMedications;
    }

    public void setListOfMedications(List<MedicationDTO> listOfMedications) {
        this.listOfMedications = listOfMedications;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public PatientDTO getPatientDTO() {
        return patientDTO;
    }

    public void setPatientDTO(PatientDTO patientDTO) {
        this.patientDTO = patientDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicationPlanDTO that = (MedicationPlanDTO) o;
        return id_medicationPlan.equals(that.id_medicationPlan) &&
                name.equals(that.name) &&
                intakeIntervals.equals(that.intakeIntervals) &&
                period.equals(that.period) &&
                listOfMedications.equals(that.listOfMedications);

    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id_medicationPlan, name, intakeIntervals, period, listOfMedications);
    }
}
