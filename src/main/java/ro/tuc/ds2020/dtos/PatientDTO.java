package ro.tuc.ds2020.dtos;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;
import java.util.UUID;

public class PatientDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private UUID id_patient;
    private String name;
    private Date birthDate;
    private String address;
    private String gender;
    private String medicalRecord;
    private String email;
    private String password;
    private CaregiverDTO caregiver;

    public PatientDTO(){

    }

    public PatientDTO(UUID id,String name, Date birthDate, String address, String gender, String medicalRecord,String email, String password, CaregiverDTO caregiver) {
        this.id_patient=id;
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
        this.email=email;
        this.password=password;
        this.caregiver=caregiver;
    }

    public PatientDTO(String name, Date birthDate, String address, String gender, String medicalRecord,String email, String password, CaregiverDTO caregiver) {
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
        this.email=email;
        this.password=password;
        this.caregiver=caregiver;
    }

    public UUID getId() {
        return id_patient;
    }

    public void setId(UUID id) {
        this.id_patient = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CaregiverDTO getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(CaregiverDTO caregiver) {
        this.caregiver = caregiver;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PatientDTO that = (PatientDTO) o;
        return id_patient.equals(that.id_patient) &&
                name.equals(that.name) &&
                birthDate.equals(that.birthDate) &&
                address.equals(that.address) &&
                gender.equals(that.gender) &&
                medicalRecord.equals(that.medicalRecord) &&
                email.equals(that.email) &&
                password.equals(that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id_patient, name, birthDate, address, gender, medicalRecord, email, password);
    }
}