package ro.tuc.ds2020.dtos;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class MedicationDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    private UUID id_medication;
    private String name;
    private String sideEffects;
    private String dosage;


    public MedicationDTO(){

    }

    public MedicationDTO(UUID id_medication,String name,String sideEffects,String dosage) {
        this.id_medication=id_medication;
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;

    }
    public MedicationDTO(String name,String sideEffects,String dosage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dosage = dosage;

    }

    public UUID getId_medication() {
        return id_medication;
    }

    public void setId_medication(UUID id_medication) {
        this.id_medication = id_medication;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedicationDTO that = (MedicationDTO) o;
        return Objects.equals(id_medication, that.id_medication) &&
                Objects.equals(name, that.name) &&
                Objects.equals(sideEffects, that.sideEffects) &&
                Objects.equals(dosage, that.dosage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id_medication, name, sideEffects, dosage);
    }
}
