package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class MedTakenDTO  implements Serializable {
    private UUID id_medtaken;
    private UUID medicationPlan;
    private UUID medication;
    private UUID patient;
    private boolean taken;

    public MedTakenDTO(){

    }

    public MedTakenDTO(UUID medicationPlan, UUID medication, UUID patient, boolean taken) {
        this.medicationPlan = medicationPlan;
        this.medication = medication;
        this.patient = patient;
        this.taken = taken;
    }

    public MedTakenDTO(UUID id_medtaken, UUID medicationPlan, UUID medication, UUID patient, boolean taken) {
        this.id_medtaken = id_medtaken;
        this.medicationPlan = medicationPlan;
        this.medication = medication;
        this.patient = patient;
        this.taken= taken;
    }

    public UUID getId_medtaken() {
        return id_medtaken;
    }

    public void setId_medtaken(UUID id_medtaken) {
        this.id_medtaken = id_medtaken;
    }

    public UUID getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(UUID medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public UUID getMedication() {
        return medication;
    }

    public void setMedication(UUID medication) {
        this.medication = medication;
    }

    public UUID getPatient() {
        return patient;
    }

    public void setPatient(UUID patient) {
        this.patient = patient;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MedTakenDTO that = (MedTakenDTO) o;
        return taken == that.taken &&
                id_medtaken.equals(that.id_medtaken) &&
                medicationPlan.equals(that.medicationPlan) &&
                medication.equals(that.medication) &&
                patient.equals(that.patient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id_medtaken, medicationPlan, medication, patient, taken);
    }
}