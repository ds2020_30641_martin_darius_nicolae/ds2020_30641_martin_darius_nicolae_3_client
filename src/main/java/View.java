import configuration.TakingExceptions;
import ro.tuc.ds2020.dtos.MedTakenDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;


import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

public class View extends JFrame {
    JLabel label;
    Timer countdownTimer;
    int timeRemaining=10000;

    JPanel panel;
    JPanel mainPanel;
    JPanel timer;

    JPanel medicationPlan1;
    JPanel medicationPlan2;

    JPanel med1Panel;
    JPanel med2Panel;
    JPanel med3Panel;
    JPanel med4Panel;

    JLabel numeMed1;
    JLabel numeMed2;
    JLabel numeMed3;
    JLabel numeMed4;

    JLabel intervalMed1;
    JLabel intervalMed2;
    JLabel intervalMed3;
    JLabel intervalMed4;

    JButton iaMed1;
    JButton iaMed2;
    JButton iaMed3;
    JButton iaMed4;


    public View(List<MedicationPlanDTO> list){
        label=new JLabel(String.valueOf(timeRemaining),JLabel.CENTER);
        timer=new JPanel();
        timer.add(label);
        countdownTimer=new Timer(1000,new CountdownTimerListener());
        setVisible(true);
        countdownTimer.start();

        Calendar cal=Calendar.getInstance();
        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
        String current=sdf.format(cal.getTime()).substring(0,2);
        int currentInt=Integer.parseInt(current);

        panel=new JPanel();

        med1Panel =new JPanel();
        med2Panel=new JPanel();
        med3Panel =new JPanel();
        med4Panel=new JPanel();

        MedicationPlanDTO plan1=list.get(0);
        MedicationDTO med1=plan1.getListOfMedications().get(0);
        MedicationDTO med2=plan1.getListOfMedications().get(1);
        MedicationPlanDTO plan2=list.get(1);
        MedicationDTO med3=plan2.getListOfMedications().get(0);
        MedicationDTO med4=plan2.getListOfMedications().get(1);

        numeMed1=new JLabel(med1.getName());
        intervalMed1=new JLabel(plan1.getIntakeIntervals());
        iaMed1=new JButton("Take");
        iaMed1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UUID medicationPlanID=plan1.getId_medicationPlan();
                UUID medicationID=med1.getId_medication();
                UUID patientID= plan1.getPatientDTO().getId();

                String begin=intervalMed1.getText().substring(0,2);
                String end=intervalMed1.getText().substring(3,5);
                int beginInt=Integer.parseInt(begin);
                int endInt=Integer.parseInt(end);
                boolean ok=false;
                if(currentInt>=beginInt && currentInt<=endInt)
                    ok=true;
                MedTakenDTO med=new MedTakenDTO(medicationPlanID,medicationID,patientID,ok);
                try {
                    String response= DemoApplication.takeMedicationService.take(med);
                } catch (TakingExceptions takingExceptions) {
                    takingExceptions.printStackTrace();
                }
            }
        } );

        numeMed2=new JLabel(med2.getName());
        intervalMed2=new JLabel(plan1.getIntakeIntervals());
        iaMed2=new JButton("Take");
        iaMed2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UUID medicationPlanID=plan1.getId_medicationPlan();
                UUID medicationID=med2.getId_medication();
                UUID patientID= plan1.getPatientDTO().getId();

                String begin=intervalMed2.getText().substring(0,2);
                String end=intervalMed2.getText().substring(3,5);
                int beginInt=Integer.parseInt(begin);
                int endInt=Integer.parseInt(end);
                boolean ok=false;
                if(currentInt>=beginInt && currentInt<=endInt)
                    ok=true;
                MedTakenDTO med=new MedTakenDTO(medicationPlanID,medicationID,patientID,ok);
                try {
                    String response= DemoApplication.takeMedicationService.take(med);
                } catch (TakingExceptions takingExceptions) {
                    takingExceptions.printStackTrace();
                }
            }
        } );

        numeMed3=new JLabel(med3.getName());
        intervalMed3=new JLabel(plan2.getIntakeIntervals());
        iaMed3=new JButton("Take");
        iaMed3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UUID medicationPlanID=plan2.getId_medicationPlan();
                UUID medicationID=med3.getId_medication();
                UUID patientID= plan2.getPatientDTO().getId();

                String begin=intervalMed3.getText().substring(0,2);
                String end=intervalMed3.getText().substring(3,5);
                int beginInt=Integer.parseInt(begin);
                int endInt=Integer.parseInt(end);
                boolean ok=false;
                if(currentInt>=beginInt && currentInt<=endInt)
                    ok=true;
                MedTakenDTO med=new MedTakenDTO(medicationPlanID,medicationID,patientID,ok);
                try {
                    String response= DemoApplication.takeMedicationService.take(med);
                } catch (TakingExceptions takingExceptions) {
                    takingExceptions.printStackTrace();
                }
            }
        } );

        numeMed4=new JLabel(med4.getName());
        intervalMed4=new JLabel(plan2.getIntakeIntervals());
        iaMed4=new JButton("Take");
        iaMed4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UUID medicationPlanID=plan2.getId_medicationPlan();
                UUID medicationID=med3.getId_medication();
                UUID patientID= plan2.getPatientDTO().getId();

                String begin=intervalMed4.getText().substring(0,2);
                String end=intervalMed4.getText().substring(3,5);
                int beginInt=Integer.parseInt(begin);
                int endInt=Integer.parseInt(end);
                boolean ok=false;
                if(currentInt>=beginInt && currentInt<=endInt)
                    ok=true;
                MedTakenDTO med=new MedTakenDTO(medicationPlanID,medicationID,patientID,ok);
                try {
                    String response= DemoApplication.takeMedicationService.take(med);
                } catch (TakingExceptions takingExceptions) {
                    takingExceptions.printStackTrace();
                }
            }
        } );

        med1Panel.add(numeMed1);
        med1Panel.add(intervalMed1);
        med1Panel.add(iaMed1);

        med2Panel.add(numeMed2);
        med2Panel.add(intervalMed2);
        med2Panel.add(iaMed2);

        med3Panel.add(numeMed3);
        med3Panel.add(intervalMed3);
        med3Panel.add(iaMed3);

        med4Panel.add(numeMed4);
        med4Panel.add(intervalMed4);
        med4Panel.add(iaMed4);

        med1Panel.setLayout(new FlowLayout());
        med2Panel.setLayout(new FlowLayout());
        med3Panel.setLayout(new FlowLayout());
        med4Panel.setLayout(new FlowLayout());

        medicationPlan1=new JPanel();
        medicationPlan1.add(med1Panel);
        medicationPlan1.add(med2Panel);
        medicationPlan1.setLayout(new GridLayout(1,2));

        medicationPlan2=new JPanel();
        medicationPlan2.add(med3Panel);
        medicationPlan2.add(med4Panel);
        medicationPlan2.setLayout(new GridLayout(1,2));

        panel.add(medicationPlan1);
        panel.add(medicationPlan2);
        panel.setLayout(new GridLayout(2,1));

        mainPanel=new JPanel();
        mainPanel.add(timer);
        mainPanel.add(panel);
        mainPanel.setLayout(new GridLayout(2,1));
        this.setTitle("Medications:");
        this.setSize(500, 500);
        this.setLocation(760, 390);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setContentPane(mainPanel);
        this.setVisible(true);
    }

    class CountdownTimerListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            if(--timeRemaining>0){
                label.setText(String.valueOf(timeRemaining));
            }else
            {
                //timeRemaining=10;
                label.setText(String.valueOf(timeRemaining));
                try {
                    List<MedicationPlanDTO> list=DemoApplication.takeMedicationService.takeAMed(UUID.fromString("3827758a-1e7a-4dde-8b21-6e411317e13c"));
                    View newView=new View(list);
                } catch (TakingExceptions takingExceptions) {
                    takingExceptions.printStackTrace();
                }
            }
        }
    }
}
