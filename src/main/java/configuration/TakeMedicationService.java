package configuration;

import ro.tuc.ds2020.dtos.MedTakenDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;

import java.util.List;
import java.util.UUID;

public interface TakeMedicationService {
    List<MedicationPlanDTO> takeAMed(UUID patientID) throws TakingExceptions;
    String take(MedTakenDTO medTaken) throws TakingExceptions;
}
