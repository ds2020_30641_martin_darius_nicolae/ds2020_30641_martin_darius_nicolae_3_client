package configuration;

public class TakingExceptions extends Exception {
    public TakingExceptions(String message){
        super(message);
    }
}